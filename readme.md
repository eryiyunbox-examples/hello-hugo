# Hugo 在 [21云盒子](https://www.21cloudbox.com/) 部署的示例

这个示例代码是通过 Hugo 官网的 [Quick Start](https://gohugo.io/getting-started/quick-start/) 创建而成。

这个静态站点已经部署在 [https://hugo.21cloudbox.com](https://hugo.21cloudbox.com)。

## 部署操作流程

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-hugo-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-hugo-in-production-server.html)